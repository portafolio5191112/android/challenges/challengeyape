package com.hamon.aplazochallenge.repositories

import androidx.test.ext.junit.runners.AndroidJUnit4
import org.junit.runner.RunWith
import org.testng.Assert.assertEquals
import org.testng.annotations.Test

@RunWith(AndroidJUnit4::class)
class DinnerRepositoryTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }
}