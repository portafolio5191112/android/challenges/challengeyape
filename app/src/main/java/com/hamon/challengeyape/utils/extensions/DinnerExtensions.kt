package com.hamon.challengeyape.utils.extensions

import com.hamon.challengeyape.data.model.DinnerData
import com.hamon.challengeyape.domain.entities.DinnerDomain
import com.hamon.challengeyape.domain.entities.LocationDomain
import com.hamon.challengeyape.presentation.models.Dinner
import com.hamon.challengeyape.presentation.models.Location

fun DinnerData.toDinnerDomain(): DinnerDomain {
    return DinnerDomain(
        name = name,
        image = image,
        description = description,
        ingredients = ingredients,
        location = LocationDomain(
            latitude = location?.latitude,
            longitude = location?.longitude,
            zoom = location?.zoom
        )
    )
}

fun DinnerDomain.toDinner(): Dinner {
    return Dinner(name = name,
        image = image,
        description = description,
        ingredients = ingredients,
        location = Location(
            latitude = location?.latitude,
            longitude = location?.longitude,
            zoom = location?.zoom
        )
    )
}