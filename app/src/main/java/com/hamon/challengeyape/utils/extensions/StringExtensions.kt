package com.hamon.challengeyape.utils.extensions

import com.google.gson.Gson

fun <T> T.toJSON(): String {
    return Gson().toJson(this)
}

inline fun <reified T> String.fromJSON(): T {
    return Gson().fromJson(this,T::class.java)
}