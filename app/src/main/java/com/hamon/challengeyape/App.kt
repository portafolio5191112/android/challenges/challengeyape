package com.hamon.challengeyape

import android.app.Application
import com.hamon.challengeyape.core.di.clients
import com.hamon.challengeyape.core.di.datasource
import com.hamon.challengeyape.core.di.repositories
import com.hamon.challengeyape.core.di.useCases
import com.hamon.challengeyape.core.di.viewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class App : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@App)
            androidFileProperties()
            modules(
                listOf(
                    clients,
                    datasource,
                    repositories,
                    useCases,
                    viewModel
                )
            )
        }
    }

}