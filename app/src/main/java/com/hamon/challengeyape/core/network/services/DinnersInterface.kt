package com.hamon.challengeyape.core.network.services

import com.hamon.challengeyape.data.model.DinnerData
import com.hamon.challengeyape.core.network.DINNERS
import retrofit2.Response
import retrofit2.http.GET

interface DinnersInterface {

    @GET(DINNERS)
    suspend fun getDinners(): Response<MutableList<DinnerData>>

}