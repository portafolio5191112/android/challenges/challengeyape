package com.hamon.challengeyape.core.network.events

sealed class ServiceResponse {
    class Success<T>(val data: T) : ServiceResponse()
    class Error(val message: String) : ServiceResponse()
}