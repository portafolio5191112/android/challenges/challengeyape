package com.hamon.challengeyape.core.di

import com.hamon.challengeyape.core.network.API
import com.hamon.challengeyape.core.network.services.DinnersInterface
import com.hamon.challengeyape.data.datasource.DinnerDatasource
import com.hamon.challengeyape.data.datasource.DinnerDatasourceImpl
import com.hamon.challengeyape.data.repositories.DinnerRepositoryImpl
import com.hamon.challengeyape.domain.repositories.DinnersRepository
import com.hamon.challengeyape.domain.use_case.GetDinnersUseCase
import com.hamon.challengeyape.domain.use_case.GetDinnersUseCaseImpl
import com.hamon.challengeyape.presentation.view_models.DinnerHomeViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val clients = module {
    single { API.getServices<DinnersInterface>() }
}

val datasource = module {
    single<DinnerDatasource> { DinnerDatasourceImpl(get()) }
}

val repositories = module {
    single<DinnersRepository> { DinnerRepositoryImpl(get()) }
}

val useCases = module {
    single<GetDinnersUseCase> { GetDinnersUseCaseImpl(get()) }
}

val viewModel = module {
    viewModel { DinnerHomeViewModel(get()) }
}