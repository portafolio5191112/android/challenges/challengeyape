package com.hamon.challengeyape.presentation.models

data class Location(
    val latitude: Double?,
    val longitude: Double?,
    val zoom: Int?
)