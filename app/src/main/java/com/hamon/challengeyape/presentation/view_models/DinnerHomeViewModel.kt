package com.hamon.challengeyape.presentation.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.hamon.challengeyape.core.network.events.ServiceResponse
import com.hamon.challengeyape.domain.entities.DinnerDomain
import com.hamon.challengeyape.domain.use_case.GetDinnersUseCase
import com.hamon.challengeyape.presentation.events.PresentationResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class DinnerHomeViewModel(private val getDinnersUseCase: GetDinnersUseCase) : ViewModel() {

    private val _presentationResponse: MutableLiveData<PresentationResponse> by lazy {
        MutableLiveData()
    }
    val presentationResponse: LiveData<PresentationResponse> get() = _presentationResponse

    private val dinnerListCache: MutableList<DinnerDomain> = mutableListOf()

    fun getDinners() {
        _presentationResponse.value = PresentationResponse.Loading
        viewModelScope.launch(Dispatchers.IO) {
            val result = getDinnersUseCase.invoke()
            handleResult(result)
        }
    }

    private fun handleResult(result: ServiceResponse) {
        when (result) {
            is ServiceResponse.Success<*> -> {
                val dinnerList = result.data as MutableList<DinnerDomain>
                dinnerListCache.clear()
                dinnerListCache.addAll(dinnerList)
                _presentationResponse.postValue(PresentationResponse.Success(data = result.data))
            }

            is ServiceResponse.Error -> {
                _presentationResponse.postValue(PresentationResponse.Error(message = result.message))
            }
        }
    }

    fun searchDinner(query: String) {
        if (query.isNotEmpty() || query.isNotBlank()) {
            val filterList =
                dinnerListCache.filter {
                    it.name?.lowercase()
                        ?.contains(query.lowercase()) == true || it.ingredients?.any { ingredient ->
                        ingredient.lowercase().contains(query.lowercase())
                    } == true
                }
            _presentationResponse.postValue(PresentationResponse.Success(data = filterList))
        } else {
            _presentationResponse.postValue(PresentationResponse.Success(data = dinnerListCache))
        }
    }

}