package com.hamon.challengeyape.presentation.screens.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.core.os.bundleOf
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.hamon.challengeyape.R
import com.hamon.challengeyape.databinding.FragmentDinnerHomeBinding
import com.hamon.challengeyape.domain.entities.DinnerDomain
import com.hamon.challengeyape.presentation.events.PresentationResponse
import com.hamon.challengeyape.presentation.models.Dinner
import com.hamon.challengeyape.presentation.view_binding.DinnerItem
import com.hamon.challengeyape.presentation.view_models.DinnerHomeViewModel
import com.hamon.challengeyape.utils.extensions.hide
import com.hamon.challengeyape.utils.extensions.show
import com.hamon.challengeyape.utils.extensions.toDinner
import com.hamon.challengeyape.utils.extensions.toJSON
import com.xwray.groupie.GroupieAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class DinnerHomeFragment : Fragment() {

    private val binding: FragmentDinnerHomeBinding by lazy {
        FragmentDinnerHomeBinding.inflate(layoutInflater)
    }

    private val viewModel by viewModel<DinnerHomeViewModel>()

    private val adapter by lazy {
        GroupieAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpRecycler()
        setupObservable()
        setupTextOnChange()
        viewModel.getDinners()
    }

    private fun setUpRecycler() {
        binding.rvDinner.adapter = adapter
    }

    private fun moveToDinnerDetail(dinner: Dinner) {
        findNavController().navigate(
            R.id.fragment_dinner_detail,
            bundleOf(Pair(DINNER_SELECTED, dinner.toJSON()))
        )
    }

    private fun setupObservable() {
        viewModel.presentationResponse.observe(viewLifecycleOwner) {
            handleResponse(it)
        }
    }

    private fun handleResponse(presentationResponse: PresentationResponse) {
        when (presentationResponse) {
            is PresentationResponse.Success<*> -> handleSuccess(presentationResponse.data as MutableList<DinnerDomain>)
            is PresentationResponse.Error -> handleError(presentationResponse.message)
            is PresentationResponse.Loading -> showLoader()
            else -> {}
        }
    }

    private fun showLoader() {
        binding.apply {
            homeLayout.hide()
            loaderHome.root.show()
        }
    }

    private fun hideLoader() {
        binding.apply {
            homeLayout.show()
            loaderHome.root.hide()
        }
    }

    private fun handleSuccess(dinnerList: MutableList<DinnerDomain>) {
        adapter.clear()
        adapter.addAll(dinnerList.map { dinnerDomain ->
            DinnerItem(dinnerDomain.toDinner()) { dinnerTap ->
                moveToDinnerDetail(dinnerTap)
            }
        })
        hideLoader()
    }

    private fun handleError(message: String) {
        Snackbar.make(requireContext(), requireView(), message, Snackbar.LENGTH_SHORT)
            .setBackgroundTint(
                ContextCompat.getColor(
                    requireContext(),
                    android.R.color.holo_red_dark
                )
            )
            .setTextColor(
                ContextCompat.getColor(
                    requireContext(),
                    android.R.color.white
                )
            ).show()
    }

    private fun setupTextOnChange() {
        binding.apply {
            tietSearch.doOnTextChanged { text, _, _, _ ->
                viewModel.searchDinner(text.toString())
            }
        }
    }

    companion object {
        const val DINNER_SELECTED = "DINNER_SELECTED"
    }

}