package com.hamon.challengeyape.presentation.models

import com.hamon.challengeyape.domain.entities.LocationDomain

data class Dinner(
    val description: String?,
    val image: String?,
    val location: Location?,
    val ingredients: MutableList<String>?,
    val name: String?
)