package com.hamon.challengeyape.presentation.screens.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.hamon.challengeyape.R
import com.hamon.challengeyape.databinding.FragmentDinnerMapBinding
import com.hamon.challengeyape.presentation.models.Dinner
import com.hamon.challengeyape.utils.extensions.fromJSON
import com.hamon.challengeyape.utils.extensions.hide
import com.hamon.challengeyape.utils.extensions.letIfAllNotNull
import com.hamon.challengeyape.utils.extensions.show

class DinnerMapFragment : Fragment() {

    private val binding: FragmentDinnerMapBinding by lazy {
        FragmentDinnerMapBinding.inflate(layoutInflater)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment?
        mapFragment?.getMapAsync { googleMap ->
            arguments?.getString(DinnerHomeFragment.DINNER_SELECTED)?.let {
                val dinner = it.fromJSON<Dinner>()
                letIfAllNotNull(
                    dinner.location?.latitude,
                    dinner.location?.longitude,
                    dinner.location?.zoom
                ) { location ->
                    val dinnerLocation = LatLng(location[0] as Double, location[1] as Double)
                    googleMap.addMarker(MarkerOptions().position(dinnerLocation).title(dinner.name))
                    googleMap.animateCamera(
                        CameraUpdateFactory.newLatLngZoom(
                            dinnerLocation,
                            (location[2] as Int).toFloat()
                        )
                    )
                }
            }
        }
    }

    private fun showLoader() {
        binding.apply {
            loaderMap.root.show()
            containerMap.hide()
        }
    }

    private fun hideLoader() {
        binding.apply {
            loaderMap.root.hide()
            containerMap.show()
        }
    }

}