package com.hamon.challengeyape.presentation.screens.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import coil.load
import com.hamon.challengeyape.R
import com.hamon.challengeyape.databinding.FragmentDinnerDetailBinding
import com.hamon.challengeyape.presentation.models.Dinner
import com.hamon.challengeyape.presentation.screens.fragments.DinnerHomeFragment.Companion.DINNER_SELECTED
import com.hamon.challengeyape.presentation.view_binding.IngredientItem
import com.hamon.challengeyape.utils.extensions.fromJSON
import com.hamon.challengeyape.utils.extensions.hide
import com.hamon.challengeyape.utils.extensions.show
import com.xwray.groupie.GroupieAdapter

class DinnerDetailFragment : Fragment() {

    private val binding: FragmentDinnerDetailBinding by lazy {
        FragmentDinnerDetailBinding.inflate(layoutInflater)
    }

    private val adapter by lazy { GroupieAdapter() }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View = binding.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showLoader()
        setupRecycler()
        setupOnClickListener()
        arguments?.getString(DINNER_SELECTED)?.let {
            setupInformation(it.fromJSON<Dinner>())
        }
    }

    private fun setupRecycler() {
        binding.rvIngredients.adapter = adapter
    }

    private fun setupInformation(dinner: Dinner) {
        binding.apply {
            ivDinner.load(dinner.image)
            tvDinnerName.text = dinner.name
            tvDescription.text = dinner.description
            adapter.clear()
            adapter.addAll(
                dinner.ingredients?.map { IngredientItem(it) }?.toMutableList() ?: mutableListOf()
            )
        }
        hideLoader()
    }

    private fun setupOnClickListener() {
        binding.cLocation.setOnClickListener {
            arguments?.getString(DINNER_SELECTED)?.let {
                findNavController().navigate(
                    R.id.fragment_dinner_map,
                    bundleOf(Pair(DINNER_SELECTED, it))
                )
            }
        }
    }

    private fun showLoader() {
        binding.apply {
            detailLoader.root.show()
            detailView.hide()
        }
    }

    private fun hideLoader() {
        binding.apply {
            detailLoader.root.hide()
            detailView.show()
        }
    }

}