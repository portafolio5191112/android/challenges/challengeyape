package com.hamon.challengeyape.presentation.view_binding

import android.view.View
import com.hamon.challengeyape.R
import com.hamon.challengeyape.databinding.ItemIngredientBinding
import com.xwray.groupie.viewbinding.BindableItem

class IngredientItem(private val ingredient: String): BindableItem<ItemIngredientBinding>() {
    override fun bind(viewBinding: ItemIngredientBinding, position: Int) {
        viewBinding.apply {
            tvIngredient.text = ingredient
        }
    }

    override fun getLayout() = R.layout.item_ingredient

    override fun initializeViewBinding(view: View) = ItemIngredientBinding.bind(view)
}