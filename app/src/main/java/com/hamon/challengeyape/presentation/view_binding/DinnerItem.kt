package com.hamon.challengeyape.presentation.view_binding

import android.view.View
import coil.load
import com.hamon.challengeyape.R
import com.hamon.challengeyape.databinding.ItemDinnerBinding
import com.hamon.challengeyape.presentation.models.Dinner
import com.xwray.groupie.viewbinding.BindableItem

class DinnerItem(private val dinner: Dinner, private val onTapDinner: (Dinner) -> Unit) :
    BindableItem<ItemDinnerBinding>() {
    override fun bind(viewBinding: ItemDinnerBinding, position: Int) {
        viewBinding.apply {
            ivDinner.load(dinner.image)
            tvDinnerName.text = dinner.name
            cDinner.setOnClickListener {
                onTapDinner(dinner)
            }
        }
    }

    override fun getLayout() = R.layout.item_dinner

    override fun initializeViewBinding(view: View) = ItemDinnerBinding.bind(view)
}