package com.hamon.challengeyape.presentation.events

sealed class PresentationResponse {
    data object Init: PresentationResponse()
    data object Loading: PresentationResponse()
    class Success<T>(val data: T) : PresentationResponse()
    class Error(val message: String) : PresentationResponse()
}