package com.hamon.challengeyape.data.datasource

import com.hamon.challengeyape.core.network.services.DinnersInterface
import com.hamon.challengeyape.data.model.DinnerData
import retrofit2.Response

interface DinnerDatasource {
    suspend fun getDinners(): Response<MutableList<DinnerData>>
}

class DinnerDatasourceImpl(private val dinnerServices: DinnersInterface): DinnerDatasource {
    override suspend fun getDinners(): Response<MutableList<DinnerData>> {
        return dinnerServices.getDinners()
    }

}