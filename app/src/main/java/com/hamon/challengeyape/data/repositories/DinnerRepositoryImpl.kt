package com.hamon.challengeyape.data.repositories

import com.hamon.challengeyape.core.network.events.ServiceResponse
import com.hamon.challengeyape.data.datasource.DinnerDatasource
import com.hamon.challengeyape.domain.repositories.DinnersRepository
import com.hamon.challengeyape.utils.extensions.toDinnerDomain

class DinnerRepositoryImpl(private val datasource: DinnerDatasource) : DinnersRepository {
    override suspend fun getDinners(): ServiceResponse {
        return try {
            val result = datasource.getDinners()
            if (result.isSuccessful) {
                ServiceResponse.Success(
                    data = result.body()?.map { it.toDinnerDomain() }?.toMutableList()
                        ?: mutableListOf()
                )
            } else {
                ServiceResponse.Error(message = "Error en el servicio")
            }
        } catch (exception: Exception) {
            ServiceResponse.Error(message = "Error al consumir el servicio")
        }
    }
}