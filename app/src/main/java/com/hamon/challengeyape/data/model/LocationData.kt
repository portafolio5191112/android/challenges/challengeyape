package com.hamon.challengeyape.data.model

import com.google.gson.annotations.SerializedName

data class LocationData(
    @SerializedName("latitude")
    val latitude: Double?,
    @SerializedName("longitude")
    val longitude: Double?,
    @SerializedName("zoom")
    val zoom: Int?
)