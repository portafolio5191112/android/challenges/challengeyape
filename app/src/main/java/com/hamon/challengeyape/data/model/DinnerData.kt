package com.hamon.challengeyape.data.model


import com.google.gson.annotations.SerializedName

data class DinnerData(
    @SerializedName("description")
    val description: String?,
    @SerializedName("ingredients")
    val ingredients: MutableList<String>?,
    @SerializedName("image")
    val image: String?,
    @SerializedName("location")
    val location: LocationData?,
    @SerializedName("name")
    val name: String?
)