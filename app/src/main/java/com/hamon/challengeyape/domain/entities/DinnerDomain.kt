package com.hamon.challengeyape.domain.entities

data class DinnerDomain(
    val description: String?,
    val ingredients: MutableList<String>?,
    val image: String?,
    val location: LocationDomain?,
    val name: String?
)