package com.hamon.challengeyape.domain.repositories

import com.hamon.challengeyape.core.network.events.ServiceResponse

interface DinnersRepository {
    suspend fun getDinners(): ServiceResponse
}