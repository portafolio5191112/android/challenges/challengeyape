package com.hamon.challengeyape.domain.entities

data class LocationDomain(
    val latitude: Double?,
    val longitude: Double?,
    val zoom: Int?
)