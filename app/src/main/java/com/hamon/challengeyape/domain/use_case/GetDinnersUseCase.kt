package com.hamon.challengeyape.domain.use_case

import com.hamon.challengeyape.core.network.events.ServiceResponse
import com.hamon.challengeyape.domain.repositories.DinnersRepository

interface GetDinnersUseCase {
    suspend operator fun invoke(): ServiceResponse
}

class GetDinnersUseCaseImpl(private val repository: DinnersRepository) : GetDinnersUseCase {
    override suspend fun invoke(): ServiceResponse {
        return repository.getDinners()
    }

}