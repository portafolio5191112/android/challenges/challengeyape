### Descripción
La siguiente aplicación tiene tres pantallas principales:
- Home: Cuenta con una lista y un serch en el cual se puede realizar busquedas con el nombre del platillo o con alguno de sus ingredientes.
- Details: Cuenta con una descripión de que es el platillo, la imagen del platillo y sus ingredientes, ademas de un boton que nos lleva al mapa.
- Map: Aqui nos muestra el pais de origen del platillo, tiene un pin el cual al darle tap se muestra el nombre del platillo.

### Arquitectura
Para la arquitectura se ocupó una mezcla de MVVM, MVI y Clean Architecture. El MVVM y Clean se ocupa para poder separar las capas, y que sea mas entendible. El segundo se ocupa para poder manejar eventos, y pasar información entre capas.

### Design Pattern
Para esta parte se ocuparon los sigientes patrones:
- Singleton: este nos ayuda a generar una sola instancia, esto es util cuando ocupamos DI.
- State: Es util junto con MVI y las Sealed classes para poder pasan informacion atraves de las capas.
- Observer/Listener: Lo ocupo para poder detonar eventos de la capa de presentación al VM, y posteriormente, poder estar observando algun cambio de información una vez que se haya consumido los servicios.
- Adapter: Aunque esta implicitamente dentro de grupie, nos ayuda a poder parcear la informacion de un dato a otro para que se pueda visualizar los datos en la UI.

### Librerias
Para las librerias se ocuparon las siguientes: 
- Grupie: Esta es una libreria sencilla de ocupar para cuando necesitamos mostrar datos en una lista, es muy simple y facil de implementar, al igual que al crear vistas.
- Koin: Para la DI, es sencillo de ocupar e implementar, ademas puede ser ocupado en modulos.
- Coil: Para poder visualizar las imagenes consumidas desde internet o localmente.
- Retrofit: Para el consumo de servicios rest.
- Navigation: Para poder mostrar, cambiar y navegar en el flujo de la app.
- Shimmer: Para poder mostrar un loader cuando se carga u obtienes la información de los servicios.
- Maps: Para poder mostrar la ubicación de los platillos.
- Material: Es una libreria que trea los proyectos android por default, contiene todos los componentes necesarios para poder realizar las vistas.

### Notas
- En la carpeta raiz tenemos una apk generada para poder descargarla y poder realizar pruebas.
- Los cambios mas actuales los tenemos en develop.
- En el siguiente enlace se puede ver el funcionamiento de la app [Video](https://drive.google.com/file/d/1nZjX3g8b2J6OH613gGX7Mbx96KUIZC6K/view?usp=sharing).